module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es2021: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 12,
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'google',
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {},
}
